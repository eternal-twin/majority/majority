import { SocketServer } from "etwin-socket-server";
import { MajorityGame } from "./MajorityGame";
import { MajorityPlayer } from "./MajorityPlayer";

async function main():Promise<void>
{
	const ss = new SocketServer<MajorityGame, MajorityPlayer>(MajorityGame, MajorityPlayer);

	ss.bindPort(SocketServer.Type.IO, 3000);
	ss.run();
}

main().catch((err:Error) => {
	console.log(err.stack);
	process.exit(1);
});