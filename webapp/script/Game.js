let $ = (s) => document.querySelector(s);

let Game = function()
{
	let socket;
	let id;
	let hasAnswered = false;

	function addBloc(id)
	{
		let playerContainer = $(".player_container");
		let div = document.createElement("div");

		div.id = id;
		div.classList.add("player_blocs");
		div.innerHTML = `<p>${id}</p>`;
		playerContainer.appendChild(div);
	}

	function applyToAllBlocs(callback)
	{
		let blocs = $(".player_container").children;

		for (let i = blocs.length - 1; i >= 0; i--) {
			callback(blocs[i]);
		}
	}

	function onClickOnAnswer(e)
	{
		socket.emit("answer", {
			answer: e.target.innerHTML
		});
		$(".mdr_button").disabled = false;
	}

	function laugh(e)
	{
		if (!hasAnswered)
		{
			hasAnswered = true;
			$(".mdr_button").disabled = true;
			socket.emit("mdr", {
				mdr: true
			});
		}
	}

	function setQuestion(question, answers)
	{
		let questionTitle = $(".question_title");
		let questionAnswers = $(".question_answers");
		let mdr_section = $(".mdr_container");
		let li;

		hasAnswered = false;

		questionTitle.innerHTML = `<p>${question}</p>`;
		questionAnswers.innerHTML = "";
		for (let i = answers.length - 1; i >= 0; i--) {
			li = document.createElement("li");
			li.innerHTML = answers[i];
			li.style.backgroundColor = getRandomColor();
			li.classList.add(answers[i].replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
			li.addEventListener("click", onClickOnAnswer);
			questionAnswers.appendChild(li);
		}
		mdr_section.innerHTML = `<button class="mdr_button" disabled>MDR</button>`
		mdr_section.addEventListener("click", laugh);
	}

	this.prepareSocket = () =>
	{
		socket = io("http://localhost:3000");

		socket.on("on_player_join", (data) => {
			addBloc(data.playerId);
		});
		socket.on("on_player_leave", (data) => {

		});
		socket.on("get_room_infos", (data) => {
			let gameContainer = $(".player_container").innerHTML = "";

			for (let i = data.players.length - 1; i >= 0; i--)
				addBloc(data.players[i]);
		});
		socket.on("get_question", (data) => {
			applyToAllBlocs((bloc) => {
				bloc.classList.remove("answered_player", "mdr_player");
				bloc.style.backgroundColor = "";
			});
			setQuestion(data.question, data.answers);
		});
		socket.on("get_player_answered", (data) => {
			console.log(data);
			$(`#${CSS.escape(data.playerId)}`).classList.add("answered_player");
		});
		socket.on("get_player_mdr", (data) => {
			document.querySelector(`#${CSS.escape(data.playerId)}`).classList.add("mdr_player");
		});
		socket.on("get_all_answers", (data) => {
			const p = data.players;

			console.log(data);
			for (let i = p.length - 1; i >= 0; i--) {
				let player_box = $(`#${CSS.escape(p[i].id)}`);
				let answer =  $(`li.${CSS.escape(p[i].answer
					.replace(/[^a-z0-9\s]/gi, '')
					.replace(/[_\s]/g, '-'))}`)
				player_box.style.backgroundColor = answer.style.backgroundColor;
				if (p[i].id === data.senderId)
					console.log(p[i].isInMajority ? "You won this turn ! :)" : "You lost this turn :(");
				if (p[i].lifes <= 0)
					player_box.classList.add("dead_player");
			}
		});
		socket.on("get_winners", (data) => {
			$(".question_container").innerHTML = `<p>Game is ended ! Winners are: ${data.winners}</p>`;
			socket.close();
		});
	}
};

function getRandomColor()
{
	return (`rgb(${rand(128, 255)}, ${rand(128, 255)}, ${rand(128, 255)})`);
}
function rand(min, max)
{
	return (~~(min + (Math.random() * (max - min))));
}